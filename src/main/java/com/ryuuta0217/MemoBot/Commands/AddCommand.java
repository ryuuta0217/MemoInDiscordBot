package com.ryuuta0217.MemoBot.Commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.ryuuta0217.MemoBot.Config.MemoData;
import com.ryuuta0217.MemoBot.Config.User;
import com.ryuuta0217.MemoBot.Config.UserConfigUtil;
import com.ryuuta0217.MemoBot.Util.FileUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static com.ryuuta0217.MemoBot.Main.waiter;

public class AddCommand extends Command {

    public AddCommand() {
        this.name = "add";
        this.aliases = new String[]{"a", "new", "n", "create", "c"};
        this.help = "メモを新しく作成します。";
        this.arguments = "[メモ名] [内容(ファイルでも可)]";
        this.guildOnly = false;
    }

    /**
     * {@link Command} の本体メソッド。
     * これは、成功した {@link #run(CommandEvent)} の応答です。
     *
     * @param event このコマンドをトリガーした {@link CommandEvent}。
     */
    @Override
    protected void execute(CommandEvent event) {
        if(event.getArgs() == null || event.getArgs().isEmpty() || event.getArgs().length() == 0) {
            setTitle(event);
        } else {
            Message msg = event.getChannel().sendMessage("お待ちください...").complete();
            String[] argS = event.getArgs().split(" ", 2);
            String title = argS[0];
            String content = argS[1];
            confirmContent(event, msg, event.getMessage(), content, title);
        }
    }

    private void setTitle(CommandEvent event) {
        AtomicReference<Message> msg = new AtomicReference<>();
        event.reply("\"新しいメモ\" のタイトルを入力してください。", msg::set);
        waiter.waitForEvent(GuildMessageReceivedEvent.class, a -> a.getAuthor().equals(event.getAuthor()) && a.getChannel().equals(event.getChannel()), a -> confirmTitle(event, msg.get(), a.getMessage()), 1, TimeUnit.MINUTES, () -> msg.get().editMessage("タイムアウトしました。\n最初からやり直してください。").queue());
    }

    private void setTitle(CommandEvent event, Message botOutput) {
        AtomicReference<Message> msg = new AtomicReference<>();
        msg.set(botOutput.editMessage("\"新しいメモ\" のタイトルを入力してください。").complete());
        waiter.waitForEvent(GuildMessageReceivedEvent.class, a -> a.getAuthor().equals(event.getAuthor()) && a.getChannel().equals(event.getChannel()), a -> confirmTitle(event, msg.get(), a.getMessage()), 1, TimeUnit.MINUTES, () -> msg.get().editMessage("タイムアウトしました。\n最初からやり直してください。").queue());
    }

    private void confirmTitle(CommandEvent event, Message botOutput, Message userInput) {
        String title = userInput.getContentRaw();
        userInput.delete().queue();
        botOutput.editMessage("メモのタイトルは \""+title+"\" でよろしいですか？ [Y/N/E]").queue();
        waiter.waitForEvent(GuildMessageReceivedEvent.class, a -> {
            if(a.getAuthor().equals(event.getAuthor()) && a.getChannel().equals(event.getChannel())) {
                if(a.getMessage().getContentRaw().matches("([YyＹｙ]|[NnＮｎ]|[YyＹｙ][EeＥｅ][SsＳｓ]|[NnＮｎ][OoＯｏ]|[EeＥｅ]|[EeＥｅ][XxＸｘ][IiＩｉ][TtＴｔ])")) {
                    return true;
                } else {
                    a.getMessage().delete().queue();
                    event.reply("入力された値(" + a.getMessage().getContentRaw() + ")は無効です: **`y` または `yes`, `n` または `no` を入力してください。**", message -> new Thread(() -> {
                        try {
                            TimeUnit.SECONDS.sleep(3);
                            message.delete().queue();
                        } catch (InterruptedException ignored) {}
                    }).start());
                }
            }
            return false;
        }, a -> {
            if(a.getMessage().getContentRaw().matches("([YyＹｙ]|[YyＹｙ][EeＥｅ][SsＳｓ])")) {
                setContent(event, botOutput, a.getMessage(), title);
            } else if(a.getMessage().getContentRaw().matches("([NnＮｎ]|[NnＮｎ][OoＯｏ])")) {
                a.getMessage().delete().queue();
                setTitle(event, botOutput);
            } else {
                event.reply("キャンセルしました。", message -> new Thread(() -> {
                    try {
                        TimeUnit.SECONDS.sleep(10);
                        message.delete().queue();
                    } catch (InterruptedException ignored) {}
                }).start());
            }
        });
    }

    private void setContent(CommandEvent event, Message botOutput, Message userInput, String memoTitle) {
        userInput.delete().queue();
        botOutput.editMessage("メモ \""+memoTitle+"\" に保存する内容を入力してください。\n" +
                "(ファイルを添付可能です。)").queue();
        waiter.waitForEvent(GuildMessageReceivedEvent.class, a -> a.getAuthor().equals(event.getAuthor()) && a.getChannel().equals(event.getChannel()), a -> confirmContent(event, botOutput, a.getMessage(), memoTitle));
    }

    private void confirmContent(CommandEvent event, Message botOutput, Message userInput, String content, String memoTitle) {
        boolean hasAttachment = userInput.getAttachments().size() != 0;
        String savedFileName = "";
        String userID = userInput.getAuthor().getId();

        String dataFol = "data/memo/users/"+userID;

        MessageBuilder mb = new MessageBuilder();
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle(memoTitle);
        eb.setDescription(content);

        if(hasAttachment) {
            String fileID = UUID.randomUUID().toString().split("-")[0];
            Message.Attachment attachment = userInput.getAttachments().get(0);
            if(FileUtil.checkDir(new File(dataFol))) {
                savedFileName = fileID + "-" + attachment.getFileName();
                if(!attachment.download(new File(dataFol, fileID + "-" + attachment.getFileName()))) {
                    event.reply("エラーが発生しました。", message -> new Thread(() -> {
                        try {
                            TimeUnit.SECONDS.sleep(10);
                            message.delete().queue();
                        } catch (InterruptedException ignored) {}
                    }).start());
                    return;
                }
            } else {
                event.reply("エラーが発生しました。", message -> new Thread(() -> {
                    try {
                        TimeUnit.SECONDS.sleep(10);
                        message.delete().queue();
                    } catch (InterruptedException ignored) {}
                }).start());
                return;
            }
        }

        //このタイミングでユーザーの送信したメッセージを削除する
        userInput.delete().complete();

        /* 後から削除できるように */
        AtomicReference<Message> msg_0 = new AtomicReference<>();
        AtomicReference<Message> msg_1 = new AtomicReference<>();
        AtomicReference<Message> msg_2 = new AtomicReference<>();

        /* メイン表示を一旦消す */
        botOutput.editMessage("~~`" + botOutput.getContentRaw() + "`~~").queue();

        /* プレビュー表示 */
        event.reply("メモはこのように表示されます:", msg_0::set);
        mb.setEmbed(eb.build());
        if(hasAttachment) {
            msg_1.set(event.getChannel().sendFile(new File(dataFol, savedFileName), savedFileName.split("-", 2)[1], mb.build()).complete());
        } else event.reply(mb.build(), msg_1::set);
        event.reply("これで保存しますか? [Y/N/E]", msg_2::set);
        /* プレビュー表示ここまで */

        String finalSavedFileName = savedFileName;

        //ユーザーの Y/N/E 入力を待つ
        waiter.waitForEvent(GuildMessageReceivedEvent.class, a -> {
            if(a.getAuthor().equals(event.getAuthor()) && a.getChannel().equals(event.getChannel())) {
                if(a.getMessage().getContentRaw().matches("([YyＹｙ]|[NnＮｎ]|[YyＹｙ][EeＥｅ][SsＳｓ]|[NnＮｎ][OoＯｏ]|[EeＥｅ]|[EeＥｅ][XxＸｘ][IiＩｉ][TtＴｔ])")) {
                    return true;
                } else {
                    a.getMessage().delete().queue();
                    event.reply("入力された値(" + a.getMessage().getContentRaw() + ")は無効です: **`y` または `yes`, `n` または `no` を入力してください。**\n" +
                            "__`終了する場合は e または exit と入力`__", message -> new Thread(() -> {
                        try {
                            TimeUnit.SECONDS.sleep(3);
                            message.delete().queue();
                        } catch (InterruptedException ignored) {}
                    }).start());
                }
            }
            return false;
        }, a -> {
            a.getMessage().delete().queue();
            msg_0.get().delete().queue();
            msg_1.get().delete().queue();
            msg_2.get().delete().queue();

            if(a.getMessage().getContentRaw().matches("([YyＹｙ]|[YyＹｙ][EeＥｅ][SsＳｓ])")) {

                if(saveMemo(userID, memoTitle, content, hasAttachment, (hasAttachment ? finalSavedFileName : null))) botOutput.editMessage("保存しました。").queue();
                else botOutput.editMessage("保存に失敗しました。").queue();

            } else if(a.getMessage().getContentRaw().matches("([NnＮｎ]|[NnＮｎ][OoＯｏ])")) {

                setContent(event, botOutput, a.getMessage(), memoTitle);

            } else {
                event.reply("キャンセルしました。", message -> new Thread(() -> {
                    try {
                        TimeUnit.SECONDS.sleep(10);
                        message.delete().queue();
                    } catch (InterruptedException ignored) {}
                }).start());
            }
        });
    }

    private void confirmContent(CommandEvent event, Message botOutput, Message userInput, String memoTitle) {
        boolean hasAttachment = userInput.getAttachments().size() != 0;
        String savedFileName = "";
        String userID = userInput.getAuthor().getId();
        String content = userInput.getContentRaw();

        String dataFol = "data/memo/users/"+userID;

        MessageBuilder mb = new MessageBuilder();
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle(memoTitle);
        eb.setDescription(content);

        if(hasAttachment) {
            String fileID = UUID.randomUUID().toString().split("-")[0];
            Message.Attachment attachment = userInput.getAttachments().get(0);
            if(FileUtil.checkDir(new File(dataFol))) {
                savedFileName = fileID + "-" + attachment.getFileName();
                if(!attachment.download(new File(dataFol, fileID + "-" + attachment.getFileName()))) {
                    event.reply("エラーが発生しました。", message -> new Thread(() -> {
                        try {
                            TimeUnit.SECONDS.sleep(10);
                            message.delete().queue();
                        } catch (InterruptedException ignored) {}
                    }).start());
                    return;
                }
            } else {
                event.reply("エラーが発生しました。", message -> new Thread(() -> {
                    try {
                        TimeUnit.SECONDS.sleep(10);
                        message.delete().queue();
                    } catch (InterruptedException ignored) {}
                }).start());
                return;
            }
        }

        //このタイミングでユーザーの送信したメッセージを削除する
        userInput.delete().complete();

        /* 後から削除できるように */
        AtomicReference<Message> msg_0 = new AtomicReference<>();
        AtomicReference<Message> msg_1 = new AtomicReference<>();
        AtomicReference<Message> msg_2 = new AtomicReference<>();

        /* メイン表示を一旦消す */
        botOutput.editMessage("~~`" + botOutput.getContentRaw() + "`~~").queue();

        /* プレビュー表示 */
        event.reply("メモはこのように表示されます:", msg_0::set);
        mb.setEmbed(eb.build());
        if(hasAttachment) {
            msg_1.set(event.getChannel().sendFile(new File(dataFol, savedFileName), savedFileName.split("-", 2)[1], mb.build()).complete());
        } else event.reply(mb.build(), msg_1::set);
        event.reply("これで保存しますか? [Y/N/E]", msg_2::set);
        /* プレビュー表示ここまで */

        String finalSavedFileName = savedFileName;

        //ユーザーの Y/N/E 入力を待つ
        waiter.waitForEvent(GuildMessageReceivedEvent.class, a -> {
            if(a.getAuthor().equals(event.getAuthor()) && a.getChannel().equals(event.getChannel())) {
                if(a.getMessage().getContentRaw().matches("([YyＹｙ]|[NnＮｎ]|[YyＹｙ][EeＥｅ][SsＳｓ]|[NnＮｎ][OoＯｏ]|[EeＥｅ]|[EeＥｅ][XxＸｘ][IiＩｉ][TtＴｔ])")) {
                    return true;
                } else {
                    a.getMessage().delete().queue();
                    event.reply("入力された値(" + a.getMessage().getContentRaw() + ")は無効です: **`y` または `yes`, `n` または `no` を入力してください。**\n" +
                            "__`終了する場合は e または exit と入力`__", message -> new Thread(() -> {
                        try {
                            TimeUnit.SECONDS.sleep(3);
                            message.delete().queue();
                        } catch (InterruptedException ignored) {}
                    }).start());
                }
            }
            return false;
        }, a -> {
            a.getMessage().delete().queue();
            msg_0.get().delete().queue();
            msg_1.get().delete().queue();
            msg_2.get().delete().queue();

            if(a.getMessage().getContentRaw().matches("([YyＹｙ]|[YyＹｙ][EeＥｅ][SsＳｓ])")) {

                if(saveMemo(userID, memoTitle, content, hasAttachment, (hasAttachment ? finalSavedFileName : null))) botOutput.editMessage("保存しました。").queue();
                else botOutput.editMessage("保存に失敗しました。").queue();

            } else if(a.getMessage().getContentRaw().matches("([NnＮｎ]|[NnＮｎ][OoＯｏ])")) {

                setContent(event, botOutput, a.getMessage(), memoTitle);

            } else {
                event.reply("キャンセルしました。", message -> new Thread(() -> {
                    try {
                        TimeUnit.SECONDS.sleep(10);
                        message.delete().queue();
                    } catch (InterruptedException ignored) {}
                }).start());
            }
        });
    }

    private boolean saveMemo(String userID, String title, String content, boolean hasAttachment, String attachmentFileName) {
        try {
            UserConfigUtil uc = new UserConfigUtil();
            User us = uc.getConfig(userID);

            if(us.memos == null) us.memos = new LinkedHashMap<>();
            MemoData md = new MemoData();
            md.content = content;
            md.hasAttachment = hasAttachment;
            md.attachmentFileName = attachmentFileName;
            us.memos.put(title, md);

            uc.writeConfig(userID, us);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
