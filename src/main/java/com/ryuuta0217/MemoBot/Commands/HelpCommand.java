package com.ryuuta0217.MemoBot.Commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.ryuuta0217.MemoBot.Main;
import net.dv8tion.jda.core.EmbedBuilder;

import java.awt.*;

import static com.ryuuta0217.MemoBot.Main.commands;

public class HelpCommand extends Command {
    public HelpCommand() {
        this.name = "help";
        this.aliases = new String[]{"?", "h"};
        this.help = "このヘルプを表示します。";
        this.guildOnly = false;
    }
    /**
     * {@link Command} の本体メソッド。
     * これは、成功した {@link #run(CommandEvent)} の応答です。
     *
     * @param event このコマンドをトリガーした {@link CommandEvent}。
     */
    @Override
    protected void execute(CommandEvent event) {
        EmbedBuilder eb = new EmbedBuilder();
        commands.forEach((key, value) -> eb.addField(event.getClient().getPrefix() + key.get(0) + " " + (key.get(1) == null ? "": key.get(1)), value, false));
        eb.addBlankField(false);
        eb.addField("※コマンド引数表記", "[] = オプション\n<> = 必須", false);
        eb.setAuthor(event.getJDA().getSelfUser().getName(), "https://gitlab.com/ryuuta0217/MemoInDiscordBot", event.getJDA().getSelfUser().getAvatarUrl());
        eb.setFooter("Version " + Main.VERSION, null);
        eb.setColor(Color.GREEN);
        event.reply(eb.build());
    }
}
