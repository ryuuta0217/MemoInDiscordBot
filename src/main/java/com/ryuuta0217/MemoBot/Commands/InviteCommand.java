package com.ryuuta0217.MemoBot.Commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class InviteCommand extends Command {
    public InviteCommand() {
        this.name = "invite";
        this.help = "Botを招待するためのリンクを表示します。";
        this.aliases = new String[]{"join", "link"};
        this.guildOnly = false;
    }
    /**
     * {@link Command} の本体メソッド。
     * これは、成功した {@link #run(CommandEvent)} の応答です。
     *
     * @param event このコマンドをトリガーした {@link CommandEvent}。
     */
    @Override
    protected void execute(CommandEvent event) {
        event.reply("https://discordapp.com/oauth2/authorize?client_id=" + event.getSelfUser().getId() + "&scope=bot&permissions=60416");
    }
}
