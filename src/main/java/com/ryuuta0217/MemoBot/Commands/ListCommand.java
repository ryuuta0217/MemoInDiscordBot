package com.ryuuta0217.MemoBot.Commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.ryuuta0217.MemoBot.Config.User;
import com.ryuuta0217.MemoBot.LocalStorage;
import net.dv8tion.jda.core.EmbedBuilder;

import java.awt.*;

public class ListCommand extends Command {
    public ListCommand() {
        this.name = "list";
        this.aliases = new String[]{"ls", "memos", "m"};
        this.help = "登録されているメモを表示します。";
        this.guildOnly = false;
    }
    /**
     * {@link Command} の本体メソッド。
     * これは、成功した {@link #run(CommandEvent)} の応答です。
     *
     * @param event このコマンドをトリガーした {@link CommandEvent}。
     */
    @Override
    protected void execute(CommandEvent event) {
        User user = LocalStorage.getUserConfig(event.getAuthor().getId());

        if(user.memos.size() == 0) {
            event.reply("メモは登録されていないようです :(");
            return;
        }

        StringBuilder keys = new StringBuilder();
        StringBuilder title = new StringBuilder();
        StringBuilder content = new StringBuilder();
        StringBuilder attachment = new StringBuilder();

        user.memos.forEach((key, value) -> {
            keys.append("`").append(key).append("`, ");
            title.append(key.length() > 10 ? key.substring(0, 10) + "..." : key).append("\n");
            content.append(value.content.length() > 10 ? value.content.replace("\n", "").substring(0, 10)+"...\n" : value.content.replace("\n", "")+"\n");
            attachment.append(value.hasAttachment ? "あり: "+(value.attachmentFileName.split("-", 2)[1].length() > 10 ? value.attachmentFileName.split("-", 2)[1].substring(0, 10)+"..." : value.attachmentFileName.split("-", 2)[1]) : "なし").append("\n");
        });

        EmbedBuilder eb = new EmbedBuilder()
                .setAuthor(event.getAuthor().getName() + "のメモ帳", null, event.getAuthor().getAvatarUrl())
                .setColor(Color.GREEN);

        if(title.toString().length() > 1024) eb.addField("タイトル", title.toString().substring(0, title.toString().substring(0, 1019).lastIndexOf("\n"))+"\n他...", true);
        else eb.addField("タイトル", title.toString(), true);

        if(content.toString().length() > 1024) eb.addField("内容", content.toString().substring(0, content.toString().substring(0, 1019).lastIndexOf("\n"))+"\n他...", true);
        else eb.addField("内容", content.toString(), true);

        if(attachment.toString().length() > 1024) eb.addField("添付ファイル", attachment.toString().substring(0, attachment.toString().substring(0, 1019).lastIndexOf("\n"))+"\n他...", true);
        else eb.addField("添付ファイル", attachment.toString(), true);

        eb.addBlankField(false);

        if(keys.toString().length() > 1024) {
            String key = keys.toString().replaceAll(", $", "");

            String s1 = key.substring(0, 1024);
            int i1 = s1.lastIndexOf(", ");
            s1 = s1.substring(0, i1);

            String s2 = key.substring(i1+1, keys.toString().length());

            eb.addField("タイトルのみ:", s1, false);
            eb.addField("タイトルのみ(2):", s2.length() > 1024 ? s2.substring(0, 1021)+"..." : s2, false);
        } else {
            eb.addField("タイトルのみ:", keys.toString().replaceAll(", $", ""), false);
        }

        event.reply(eb.build());
    }
}
