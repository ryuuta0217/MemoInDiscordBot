package com.ryuuta0217.MemoBot.Commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.ryuuta0217.MemoBot.Config.MemoData;
import com.ryuuta0217.MemoBot.Config.User;
import com.ryuuta0217.MemoBot.LocalStorage;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.MessageEmbed;

import java.io.File;
import java.util.LinkedHashMap;

public class ReadCommand extends Command {
    public ReadCommand() {
        this.name = "read";
        this.aliases = new String[]{"r", "load", "l", "get", "g"};
        this.help = "登録されているメモを読み込みます。";
        this.arguments = "<メモのタイトル>";
        this.guildOnly = false;
    }
    /**
     * {@link Command} の本体メソッド。
     * これは、成功した {@link #run(CommandEvent)} の応答です。
     *
     * @param event このコマンドをトリガーした {@link CommandEvent}。
     */
    @Override
    protected void execute(CommandEvent event) {
        User user = LocalStorage.getUserConfig(event.getAuthor().getId());

        if(user.memos == null || user.memos.size() == 0) {
            event.reply("メモが登録されてません。\n" +
                    "m!add を使用してメモを追加してから、実行してください。");
            return;
        }

        LinkedHashMap<String, MemoData> memos = user.memos;
        if(!memos.containsKey(event.getArgs())) {
            event.reply("メモ " + event.getArgs() + " は存在しません。");
            return;
        }

        MemoData data = memos.get(event.getArgs());

        MessageEmbed me = new EmbedBuilder()
                .setTitle(event.getArgs())
                .setDescription(data.content)
                .build();

        if(data.hasAttachment) event.getChannel().sendFile(new File("data/memo/users/"+event.getAuthor().getId(), data.attachmentFileName), data.attachmentFileName.split("-", 2)[1], new MessageBuilder().setEmbed(me).build()).queue();
        else event.reply(me);
    }
}
