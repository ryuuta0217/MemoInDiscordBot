package com.ryuuta0217.MemoBot.Config;

import static com.ryuuta0217.MemoBot.Config.BotConfigUtil.ConfigVersion;

public class Bot {
    public int config_version = ConfigVersion;
    public String Token;
    public String Game;
    public String Prefix;
    public String AltPrefix;
    public boolean AutoReconnect;
    public int reconnect_delay;
}
