package com.ryuuta0217.MemoBot.Config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.json.JSONObject;

import java.io.*;
import java.nio.file.Files;
import java.util.stream.Collectors;

public class BotConfigUtil {
    static int ConfigVersion = 1;
    String FilePos = "./config";
    File ConfigRoot = new File(FilePos);
    String FileName = "config.json";
    File ConfigFile = new File(FilePos, FileName);

    public Bot getConfig() throws IOException {
        boolean isDefaultSettings = false;

        if(!checkDaF()) return null;

        String s = Files.lines(ConfigFile.toPath()).collect(Collectors.joining("\n"));
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

        if(s == null || s.isEmpty()) {
            Bot n = new Bot();
            n.Token = "YOUR_TOKEN_PASTE_HERE";
            n.Prefix = "PREFIX_PUT_HERE";
            n.AltPrefix = "ALT_PREFIX_PUT_HERE";
            n.AutoReconnect = true;
            n.reconnect_delay = 60;
            n.config_version = ConfigVersion;
            n.Game = "%prefix%help | %version%";
            s = mapper.writeValueAsString(writeConfig(n));
            isDefaultSettings = true;
        }

        //Config Version Check
        JSONObject jsonObj = new JSONObject(s);
        if(jsonObj.getInt("config_version") == ConfigVersion) {
            if(!isDefaultSettings) return mapper.readValue(s, Bot.class);
            else return null;
        } else {
            throw new NullPointerException("Sorry, Config Version " + jsonObj.getInt("config_version") + " is not supported.\ntry again.");
        }
    }

    public Bot writeConfig(Bot config) {
        try {
            ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
            if(checkDaF()) {
                FileWriter fw = new FileWriter(ConfigFile.getPath());
                PrintWriter pw = new PrintWriter(new BufferedWriter(fw));
                pw.println(mapper.writeValueAsString(config));
                pw.close();
                return getConfig();
            }
        } catch(Exception ignored) {
            return null;
        }
        return null;
    }

    public boolean checkDaF() {
        try {
            if(!ConfigRoot.exists()) ConfigRoot.mkdirs();
            if(!ConfigFile.exists()) ConfigFile.createNewFile();
            return true;
        } catch(Exception e) {
            return false;
        }
    }
}

