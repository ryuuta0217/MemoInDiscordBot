package com.ryuuta0217.MemoBot.Config;

import java.util.LinkedHashMap;

import static com.ryuuta0217.MemoBot.Config.UserConfigUtil.userConfigVersion;

public class User {
    public int uconfig_version = userConfigVersion;
    public LinkedHashMap<String, MemoData> memos;
}
