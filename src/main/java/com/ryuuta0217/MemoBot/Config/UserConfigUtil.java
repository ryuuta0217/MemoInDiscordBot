package com.ryuuta0217.MemoBot.Config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.ryuuta0217.MemoBot.LocalStorage;
import org.json.JSONObject;

import java.io.*;
import java.nio.file.Files;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;

public class UserConfigUtil {
    static int userConfigVersion = 1;
    private String filePos = "./data/memo/users";
    private String fileName = "meta.json";

    public User getConfig(String userId) throws IOException {
        File root = new File(filePos, userId);
        File CF = new File(root,fileName);

        if(!checkDaF(root, CF)) return null;

        String s = Files.lines(CF.toPath()).collect(Collectors.joining("\n"));
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

        if(s == null || s.isEmpty()) {
            User u = new User();
            u.memos = new LinkedHashMap<>();
            s = mapper.writeValueAsString(writeConfig(userId, u));
        }

        //Config Version Check
        JSONObject jsonObj = new JSONObject(s);
        if(jsonObj.getInt("uconfig_version") == userConfigVersion) {
            return mapper.readValue(s, User.class);
        } else {
            throw new NullPointerException("Sorry, Config Version " + jsonObj.getInt("uconfig_version") + " is not supported.\ntry again.");
        }
    }

    public User writeConfig(String userId, User user) {
        File root = new File(filePos, userId);
        File CF = new File(root, fileName);
        try {
            ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
            if(checkDaF(root, CF)) {
                FileWriter fw = new FileWriter(CF.getPath());
                PrintWriter pw = new PrintWriter(new BufferedWriter(fw));
                pw.println(mapper.writeValueAsString(user));
                pw.close();
                LocalStorage.setUserConfig(userId, user);
                return getConfig(userId);
            }
        } catch(Exception ignored) {
            return null;
        }
        return null;
    }

    private boolean checkDaF(File root, File CF) {
        try {
            if(!root.exists()) root.mkdirs();
            if(!CF.exists()) CF.createNewFile();
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}
