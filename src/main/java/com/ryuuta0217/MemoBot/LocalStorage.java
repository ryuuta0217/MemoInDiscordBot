package com.ryuuta0217.MemoBot;

import com.ryuuta0217.MemoBot.Config.User;

import java.util.HashMap;
import java.util.Map;

public class LocalStorage {
    private static Map<String, User> userConfigs = new HashMap<>();

    public static User getUserConfig(String userID) {
        if(userConfigs.containsKey(userID)) {
            return userConfigs.get(userID);
        } else {
            throw new NullPointerException("ユーザーID " + userID + " のデータは見つかりませんでした。");
        }
    }

    public static boolean setUserConfig(String userID, User data) {
        try {
            userConfigs.put(userID, data);
            return true;
        } catch(Exception e) {
            return false;
        }
    }
}
