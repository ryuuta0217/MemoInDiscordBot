package com.ryuuta0217.MemoBot;

import com.jagrosh.jdautilities.command.CommandClient;
import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.ryuuta0217.MemoBot.Commands.*;
import com.ryuuta0217.MemoBot.Config.Bot;
import com.ryuuta0217.MemoBot.Config.BotConfigUtil;
import com.ryuuta0217.MemoBot.listeners.ReadyListener;
import net.dv8tion.jda.core.JDABuilder;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Main {
    public static Map<List<String>, String> commands;
    public static CommandClient client;
    public static EventWaiter waiter;
    public static String VERSION = "1.0.2";
    public static String AUTHOR = "ryuuta0217";

    public static void main(String[] args) throws LoginException {
        String token = System.getenv("JDA_MEMOBOT_TOKEN");

        CommandClientBuilder ccb = new CommandClientBuilder();
        ccb.setPrefix("m!");
        ccb.useHelpBuilder(false);
        ccb.setOwnerId("249869841706516481");
        waiter = new EventWaiter();

        BotConfigUtil configUtil = new BotConfigUtil();
        Bot config;
        try {
            config = configUtil.getConfig();
        } catch (IOException e) {
            System.out.println("Configの読み込みに失敗しました。");
            return;
        }

        if(config == null) {
            System.out.println("Configを ./config/config.json に生成しました。設定を行ってください。");
            return;
        }

        if(token == null)
            if(config.Token == null || config.Token.isEmpty() || config.Token.equalsIgnoreCase("YOUR_TOKEN_PASTE_HERE")) {
                System.out.println("Tokenが設定されていません。config.jsonファイルを編集して、設定してください。");
                return;
            } else {
                token = config.Token;
            }


        ccb.addCommands(new HelpCommand(), new AddCommand(), new ReadCommand(), new ListCommand(), new InviteCommand());
        client = ccb.build();
        new JDABuilder()
                .setToken(token)
                .addEventListener(waiter, client, new ReadyListener())
                .setAudioEnabled(false)
                .setAutoReconnect(true)
                .setMaxReconnectDelay(60)
                .build();
    }
}
