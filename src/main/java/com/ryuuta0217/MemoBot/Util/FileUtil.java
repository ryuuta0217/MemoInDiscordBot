package com.ryuuta0217.MemoBot.Util;

import java.io.File;
import java.io.IOException;

public class FileUtil {
    public static boolean checkDir(File file) {
        if(file.exists()) return true;
        return file.mkdirs();
    }

    public static boolean checkFile(File file) {
        if(file.exists()) return true;
        try {
            return file.createNewFile();
        } catch (IOException ignored) {}

        return false;
    }
}
