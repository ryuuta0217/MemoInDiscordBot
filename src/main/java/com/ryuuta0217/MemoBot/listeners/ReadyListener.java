package com.ryuuta0217.MemoBot.listeners;

import com.ryuuta0217.MemoBot.Config.UserConfigUtil;
import com.ryuuta0217.MemoBot.LocalStorage;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashMap;

import static com.ryuuta0217.MemoBot.Main.client;
import static com.ryuuta0217.MemoBot.Main.commands;

public class ReadyListener extends ListenerAdapter {
    @Override
    public void onReady(ReadyEvent event) {
        commands = new LinkedHashMap<>();
        client.getCommands().forEach(command -> commands.put(Arrays.asList(command.getName(), command.getArguments()), command.getHelp()));

        event.getJDA().getUsers().forEach(user -> {
            try {
                LocalStorage.setUserConfig(user.getId(), new UserConfigUtil().getConfig(user.getId()));
            } catch (IOException ignored) {}
        });
    }
}
